<?php
require_once __DIR__.'/core/function.php';
if(isPost()){
    if(!empty($_POST['sign_in'])){
        if(!empty($_POST['login']) && !empty($_POST['password'])){
            if(checkUser($_POST['login'], $_POST['password'])){
                header("Location: index.php");
            }
        }
        $str = "Такой пользователь не существует, либо неверный пароль.";
    }

    if(!empty($_POST['register'])){
        if(!empty($_POST['login']) && !empty($_POST['password'])){
            if(register($_POST['login'], $_POST['password'])){
                header("Location: index.php");
            }else{
                $str = "Такой пользователь уже существует в базе данных.";
            }
        }else{
            $str = "Ошибка регистрации. Введите все необходимые данные:";
        }
    }
}else{
    $str = "Введите данные для регистрации или войдите, если уже регистрировались:";
}
$twig = cmp_Twig();
echo $twig->render('register.twig', ['str' => $str]);
?>