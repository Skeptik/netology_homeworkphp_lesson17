<?php
require_once __DIR__.'/core/function.php';
if (!isAuthorised()) {
    echo "<a href='register.php'>Войдите на сайт</a>";
    die;
}
require_once __DIR__.'/core/connectBD.php';
$flagEdit = false;
$str = '';
$id = 0;
if(!empty($_GET['id']) && !empty($_GET['action'])){
    switch ($_GET['action']) {
        case 'edit':
        $flagEdit = true;
        $id = $_GET['id'];
        $str = edit($id);
        break;
        case 'done':
        $check = done();
        break;
        case 'delete':
        $check = delete();
        break;
    }
    if(!$flagEdit){
        $db->query($check);
    }
}

if(!empty($_POST['description']) && $_POST['save'] =='Сохранить'){
    $spec = special($_POST['description']);
    if(saveTask($spec, $id)){
        $flagEdit = false;
    }
}

if(!empty($_POST['assigned_user_id'])){
    updateUserId($_POST['assigned_user_id']);
}

if (!empty($_POST['description']) && $_POST['save'] =='Добавить'){
    $spec = special($_POST['description']);
    if(!addUserTask($spec))
        die();
}

$twig = cmp_Twig();
echo $twig->render('userTask.twig',
    ['user' => $_SESSION['user'], 'flag' => $flagEdit, 'arrTable' => userTask(),
    'arrUsers' => allUsers(), 'arrUserTask' => allTasksUsers(), 'str' => $str]);
?>