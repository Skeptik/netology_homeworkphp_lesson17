<?php

/* userTask.twig */
class __TwigTemplate_070c57510f13aaca807bc7958ca5ee2d1f87d79470b552df232a1de0b73c2116 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Document</title>
    <style>
    table {
        border-spacing: 0;
        border-collapse: collapse;
        font-size: 18px;
    }

    table td, table th {
        border: 1px solid #ccc;
        padding: 5px;
    }

    table th {
        background: #eee;
    }
    div {
        padding: 7px;
        padding-right: 20px;
        border: solid 1px black;
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 13pt;
        background: #E6E6FA;
       }
</style>
</head>
<body>
<h1>Здравствуйте, ";
        // line 32
        echo twig_escape_filter($this->env, ($context["user"] ?? null), "html", null, true);
        echo "! Вот ваш список дел:</h1>
<div style=\"float: left\">
    ";
        // line 34
        if ((($context["flag"] ?? null) == false)) {
            // line 35
            echo "    <form method=\"POST\" action=\"#\" name=\"myform\">
        <input type=\"text\" name=\"description\" placeholder=\"Описание задачи\" value=\"\" />
        <input type=\"submit\" name=\"save\" value=\"Добавить\" />
    </form>
    ";
        } else {
            // line 40
            echo "    <form method=\"POST\" action=\"#\" name=\"myform\">
        <input type=\"text\" name=\"description\" placeholder=\"Описание задачи\" value=\"";
            // line 41
            echo twig_escape_filter($this->env, ($context["str"] ?? null), "html", null, true);
            echo "\" />
        <input type=\"submit\" name=\"save\" value=\"Сохранить\" />
    </form>
   ";
        }
        // line 45
        echo "</div>

    <table>
      <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
        <th>Закрепить задачу за пользователем</th>
    </tr>
    <br><br><br>
";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["arrTable"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
            // line 59
            echo "     <tr>
        <td>";
            // line 60
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "description", array()), "html", null, true);
            echo "</td>
        <td>";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "date_added", array()), "html", null, true);
            echo "</td>
        <td>
            ";
            // line 63
            if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "is_done", array()) == 0)) {
                // line 64
                echo "            <span style='color: orange'>В процессе</span>
            ";
            } else {
                // line 66
                echo "            <span style='color: green'>Выполнено</span>
            ";
            }
            // line 68
            echo "        </td>
        <td>
            <a href='?id=";
            // line 70
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "id", array()), "html", null, true);
            echo "&action=edit'>Изменить</a>
            <a href='?id=";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "id", array()), "html", null, true);
            echo "&action=done'>Выполнить</a>
            <a href='?id=";
            // line 72
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "id", array()), "html", null, true);
            echo "&action=delete'>Удалить</a>
        </td>
        <td>";
            // line 74
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "assigned_name", array()), "html", null, true);
            echo "</td>
        <td>";
            // line 75
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "user_name", array()), "html", null, true);
            echo "</td>
        <td>
            <form method='POST'>
                <select name='assigned_user_id'>
                ";
            // line 79
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["arrUsers"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["ch"]) {
                // line 80
                echo "                    <option value=\"user_";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["ch"], "id", array()), "html", null, true);
                echo "-task_";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["ch"], "login", array()), "html", null, true);
                echo "</option>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ch'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 82
            echo "                </select>
            <input type='submit' name='assign' value='Переложить ответственность' />
            </form>
        </td>
        </tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "</table>

<h3>Также, посмотрите, что от Вас требуют другие люди:</h3>
    <table>
      <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
    </tr>
    <br><br><br>
    ";
        // line 101
        $context["continue"] = false;
        // line 102
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["arrUserTask"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
            // line 103
            echo "        ";
            if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "user_name", array()) == ($context["user"] ?? null))) {
                // line 104
                echo "            ";
                $context["continue"] = true;
                // line 105
                echo "        ";
            }
            // line 106
            echo "        ";
            if ( !($context["continue"] ?? null)) {
                // line 107
                echo "        <tr>
            <td>";
                // line 108
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "description", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 109
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "date_added", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 110
                if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "is_done", array()) == 0)) {
                    // line 111
                    echo "                <span style='color: orange'>В процессе</span>
                ";
                } else {
                    // line 113
                    echo "                <span style='color: green'>Выполнено</span>
                ";
                }
                // line 115
                echo "            </td>
            <td>
                <a href='?id=";
                // line 117
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "id", array()), "html", null, true);
                echo "&action=done'>Выполнить</a>
            </td>
            <td>";
                // line 119
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "assigned_name", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 120
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["t"], "user_name", array()), "html", null, true);
                echo "</td>
        </tr>
        ";
            }
            // line 123
            echo "        ";
            $context["continue"] = false;
            // line 124
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 125
        echo "</table>
<h3><a href='logout.php'>Выход</a></h3>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "userTask.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 125,  250 => 124,  247 => 123,  241 => 120,  237 => 119,  232 => 117,  228 => 115,  224 => 113,  220 => 111,  218 => 110,  214 => 109,  210 => 108,  207 => 107,  204 => 106,  201 => 105,  198 => 104,  195 => 103,  190 => 102,  188 => 101,  173 => 88,  162 => 82,  149 => 80,  145 => 79,  138 => 75,  134 => 74,  129 => 72,  125 => 71,  121 => 70,  117 => 68,  113 => 66,  109 => 64,  107 => 63,  102 => 61,  98 => 60,  95 => 59,  91 => 58,  76 => 45,  69 => 41,  66 => 40,  59 => 35,  57 => 34,  52 => 32,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "userTask.twig", "D:\\OpenServer\\OSPanel\\domains\\les17\\templates\\userTask.twig");
    }
}
