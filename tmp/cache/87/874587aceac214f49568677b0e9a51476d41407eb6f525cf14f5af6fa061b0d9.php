<?php

/* register.twig */
class __TwigTemplate_069719dfb2830fc5b983e456812bdfe553e8fe8871f2ca45913243b2f8752c12 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Авторизация</title>
</head>
<body>
<p>";
        // line 8
        echo twig_escape_filter($this->env, ($context["str"] ?? null), "html", null, true);
        echo "</p>
<form method=\"POST\" action=\"#\" name=\"myform\">
    <input type=\"text\" name=\"login\" placeholder=\"Логин\" />
    <input type=\"password\" name=\"password\" placeholder=\"Пароль\" />
    <input type=\"submit\" name=\"sign_in\" value=\"Вход\" />
    <input type=\"submit\" name=\"register\" value=\"Регистрация\" />
</form>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "register.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 8,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "register.twig", "D:\\OpenServer\\OSPanel\\domains\\les17\\templates\\register.twig");
    }
}
