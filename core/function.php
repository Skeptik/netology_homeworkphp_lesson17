<?php
session_start();

function allTasksUsers(){
    try{
        $arrUserTask = [];
        require_once __DIR__.'/connectBD.php';
        global $db;
        $results = $db->query("
            SELECT t.id, t.description, t.date_added, t.is_done, u2.login assigned_name, u1.login user_name
            FROM task t
            JOIN user u1 ON u1.id=t.user_id
            JOIN user u2 ON u2.id=t.assigned_user_id
            WHERE t.assigned_user_id =".$_SESSION['userId']
        );
        while ($row = $results->fetch(PDO::FETCH_ASSOC)){
            array_push($arrUserTask, $row);
        }
        return $arrUserTask;
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
}

function allUsers(){
    try{
        $arrUsers = [];
        require_once __DIR__.'/connectBD.php';
        global $db;
        $results = $db->query("SELECT id, login FROM user");
        while ($row = $results->fetch(PDO::FETCH_ASSOC)){
            array_push($arrUsers, $row);
        }
        return $arrUsers;
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
}

function userTask(){
    try{
        $arrUserTable = [];
        require_once __DIR__.'/connectBD.php';
        global $db;
        $results = $db->query("
            SELECT t.id, t.description, t.date_added, t.is_done, u2.login assigned_name, u1.login user_name
            FROM task t
            JOIN user u1 ON u1.id=t.user_id
            JOIN user u2 ON u2.id=t.assigned_user_id
            WHERE t.user_id =".$_SESSION['userId']
        );
        while ($row = $results->fetch(PDO::FETCH_ASSOC)){
            array_push($arrUserTable, $row);
        }
        return $arrUserTable;
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
}

function addUserTask($spec){
    try{
        require_once __DIR__.'/connectBD.php';
        global $db;
        $check = "INSERT INTO task (description, date_added, user_id, assigned_user_id)
        VALUES ('".$spec."', '".date("Y-m-d H:i:s")."', '".$_SESSION['userId']."', '".$_SESSION['userId']."')";
        $db->query($check);
        return true;
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
}

function updateUserId($assig){
    try{
        require_once __DIR__.'/connectBD.php';
        global $db;
        $data = explode('-', $assig);
            foreach ($data as $value){
                $v = explode('_', $value);
                $asocArr[$v[0]] = $v[1];
            }
        $db->query("UPDATE task SET assigned_user_id=".$asocArr['user']." WHERE id=".$asocArr['task']);
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
}

function saveTask($spec, $id){
    try{
        require_once __DIR__.'/connectBD.php';
        global $db;
        $check = "UPDATE task SET description ='".$spec."' WHERE id=".$id;
        $db->query($check);
        return true;
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
}

function edit($id){
    try{
        require_once __DIR__.'/connectBD.php';
        global $db;
        $results = $db->query('SELECT description FROM task WHERE id='.$id);
        while ($row = $results->fetch(PDO::FETCH_ASSOC)){
            return $row['description'];
        }
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
}

function delete(){
    return 'DELETE FROM task WHERE id ='.$_GET['id'];
}

function done(){
    return 'UPDATE task SET is_done = 1 WHERE id='.$_GET['id'];
}

function register($login, $password){
require_once __DIR__.'/connectBD.php';
$results = $db->query('SELECT login, password FROM user');
    while ($row = $results->fetch(PDO::FETCH_ASSOC)){
        if ($row['login'] == $login && $row['password'] == md5($password)){
            return false;
        }
    }
    $var = "INSERT INTO user(login, password) VALUES ('".$login."','".md5($password)."')";
    $db->query($var);
    $results = $db->query("SELECT id FROM user WHERE login='".$login."'");
    $row = $results->fetch(PDO::FETCH_ASSOC);
    $_SESSION['user'] = $login;
    $_SESSION['userId'] = $row['id'];
    return true;
}

function checkUser($login, $password){
    require_once __DIR__.'/connectBD.php';
    $results = $db->query('SELECT id, login, password FROM user');
    while ($row = $results->fetch(PDO::FETCH_ASSOC)){
        if ($row['login'] == $login && $row['password'] == md5($password)){
            $_SESSION['user'] = $login;
            $_SESSION['userId'] = $row['id'];
            return true;
        }
    }
    return false;
}

function isAuthorised(){
    if (!empty($_SESSION['user']) && !empty($_SESSION['userId'])){
        return true;
    }else{
        return false;
    }
}

function isPost(){
    return $_SERVER['REQUEST_METHOD'] == 'POST';
}

function getParamPost($name){
    return isset($_POST[$name]) ? $_POST[$name] : null;
}

function logout(){
    session_destroy();
}

function special($str){
    $str = htmlspecialchars($str);
    return $str;
}

function cmp_Twig(){
    require_once __DIR__.'/../vendor/autoload.php';
    $loader = new Twig_Loader_Filesystem(__DIR__.'/../templates');
    return new Twig_Environment($loader, array(
        'cache' => __DIR__.'/../tmp/cache',
        'auto_reload' => true,
    ));
}
?>